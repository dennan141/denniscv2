//
//  ExperienceViewController.swift
//  DennisCV
//
//  Created by Dennis Karlsson on 2020-02-10.
//  Copyright © 2020 Dennis Karlsson. All rights reserved.
//

import Foundation
import UIKit

class ExperienceViewController: UIViewController {
    var section = ["Work", "Education"]
    
    @IBOutlet weak var experienceTableView: UITableView!
    var work_experience: [Experience] = []
    var education_experience: [Experience] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        experienceTableView.delegate = self
        experienceTableView.dataSource = self
        
        for _ in 0..<2 {
            let work_exp = Experience(title: "Tola Plåt AB", imageName: "briefcase", years: "2014-2019", description: "WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK")
            work_experience.append(work_exp)
        }
        for _ in 0..<5{
            let edu_exp = Experience(title: "Högskolan JU", imageName: "briefcase.fill", years: "2017-Present", description: "Education, Education; Education, Education, Education; Education, Education, Education; Education, Education, Education; Education, Education, Education; Education")
            education_experience.append(edu_exp)
        }
        experienceTableView.reloadData()
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailViewController" {
            let destViewController = segue.destination as! DetailViewController
            destViewController.experienceInstance = sender as? Experience
        }
    }
}


extension ExperienceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.work_experience.count
        }
        else if section == 1{
            return self.education_experience.count
        }
        else{
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let titles = ["Work_Experience", "Education_experience"]
        return titles[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ExpCell", for: indexPath) as? ExperienceTableCell{
            var generalExperience = Experience()
            
            if indexPath.section == 0{
                generalExperience = self.work_experience[indexPath.row]
            }
            else if indexPath.section == 1 {
                generalExperience = self.education_experience[indexPath.row]
            }
            cell.experienceImage.image = UIImage(systemName: generalExperience.imageName)
            cell.experienceName.text = generalExperience.title
            cell.experienceLength.text = generalExperience.yearsOfExperience
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var cell: Experience = Experience()
        if indexPath.section == 0 {
            cell = work_experience[indexPath.row]
        }
        else if indexPath.section == 1{
            cell = education_experience[indexPath.row]
        }
        performSegue(withIdentifier: "DetailViewController", sender: cell)
    }
}
