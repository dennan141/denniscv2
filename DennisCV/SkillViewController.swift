//
//  SkillViewController.swift
//  DennisCV
//
//  Created by Dennis Karlsson on 2020-02-27.
//  Copyright © 2020 Dennis Karlsson. All rights reserved.
//

import Foundation
import UIKit

class SkillViewController: UIViewController{
    
    
    @IBAction func exitButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var skillAnimationLabel: UILabel!
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.skillAnimationLabel.alpha = 0
        UIView.animate(withDuration: 3, delay: 0, options: .curveEaseInOut, animations: {
            self.skillAnimationLabel.alpha = 1
        }, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
