//
//  Experience.swift
//  DennisCV
//
//  Created by Dennis Karlsson on 2019-11-25.
//  Copyright © 2019 Dennis Karlsson. All rights reserved.
//

import Foundation



class Experience{
    let title: String
    let imageName: String
    let yearsOfExperience: String
    let description: String
    
    init(title: String = "title", imageName: String = "default", years: String = "Many", description: String = "EXAMPLE DESCRIPTION") {
        self.title = title
        self.imageName = imageName
        self.yearsOfExperience = years
        self.description = description
    }
}
