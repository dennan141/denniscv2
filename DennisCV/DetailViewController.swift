//
//  DetailViewController.swift
//  DennisCV
//
//  Created by Dennis Karlsson on 2020-02-27.
//  Copyright © 2020 Dennis Karlsson. All rights reserved.
//

import Foundation
import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailNavigation: UINavigationItem!
    @IBOutlet weak var detailimage: UIImageView!
    @IBOutlet weak var detailHeader: UILabel!
    @IBOutlet weak var detailDate: UILabel!
    @IBOutlet weak var detailDescription: UILabel!
    
    
    var experienceInstance: Experience?
    override func viewDidLoad() {
        super.viewDidLoad()
        detailNavigation.title = experienceInstance?.title
        detailHeader.text = experienceInstance?.title
        detailDate.text = experienceInstance?.yearsOfExperience
        detailDescription.text = experienceInstance?.description
        detailimage.image = UIImage(systemName: experienceInstance?.imageName ?? "default")
    }
}
