//
//  File.swift
//  DennisCV
//
//  Created by Dennis Karlsson on 2019-11-25.
//  Copyright © 2019 Dennis Karlsson. All rights reserved.
//

import Foundation
import UIKit

class ExperienceTableCell: UITableViewCell {
    
    
    @IBOutlet weak var experienceImage: UIImageView!
    @IBOutlet weak var experienceName: UILabel!
    @IBOutlet weak var experienceLength: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
